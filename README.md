# CANTICOS API 🧐
Rest API built in python with fastAPI for application of songs from Chocó - Colombia

## Getting Started 🚀
1. Clone the repository:
```bash
git clone https://gitlab.com/rafaelramosr/canticos-api.git
```

2. Enter the folder:
```bash
cd canticos-api
```

3. Create virtual environment:
```bash
# Linux
python3 -m venv venv
```

```bash
# Windows
python -m virtualenv venv
```

4. Activate virtual environment:
```bash
# Linux
source venv/bin/activate
```

```bash
# Windows
.\venv\Scripts\activate.bat
```

5. Install the dependencies:
```bash
pip install -r requirements.txt
```

6. Create and instantiate the [database](#database-%EF%B8%8F).

7. Set the [environment variables](#environment-variables-) in the `.env` file.

8. Run the local server and you're done:
```bash
uvicorn app.main:app --reload
```

9. Open your browser at:
  - [Home](http://127.0.0.1:8000/) - `http://127.0.0.1:8000/`
  - [Documentation](http://127.0.0.1:8000/docs) - `http://127.0.0.1:8000/docs`
  - [Alternative doc](http://127.0.0.1:8000/redoc) - `http://127.0.0.1:8000/redoc`

## Utility Commands 🔧

- Create `requirements.txt` file from already installed packages
  ```bash
  pip freeze > requirements.txt
  ```

- Check for code errors
  ```bash
  flake8 app/**/**/**.py --max-line-length=88
  ```

- Format code
  ```bash
  black app/**/**/**.py
  ```

- Generate secret key
  ```bash
  openssl rand -hex 32
  ```

- Create a database image with Docker
  ```bash
  docker run --name <image_name> -p <port>:3306 -e MYSQL_ROOT_PASSWORD=<password> -e LANG=C.UTF-8 mysql:5.7.36
  ```

- Manage database from Docker
  ```bash
  docker exec -it <container_id || image_name> bash
  ```

## Set config ⚙️
### Database 🗄️
To create the project database run the `mysql -u root -p <database_name> < initdb.sql` command in the MySQL shell or copy and paste the statements from the `initdb.sql` file.

Once the database is created, initialize the tables with the statements from the `seeders` folder located at the root of the project. Run the SQL statements in the following order:
- `action_binnacle.seeder.sql`
- `category.seeder.sql`
- `departamento.seeder.sql`
- `municipio.seeder.sql`
- `permiso.seeder.sql`
- `rol.seeder.sql`
- `usuario.seeder.sql`
- `permiso_rol.seeder.sql`

### Environment variables 🌱
In the `.env` file, you must set the configuration variables to connect to the database and AWS services.

```
# .env file
AWS_ACCESS_KEY_ID=<digitalocean_spaces_key_id>
AWS_SECRET_ACCESS_KEY=<digitalocean_spaces_secret_key>

SQL_DATABASE_URL=mysql+pymysql://<db_username>:<db_password>@<db_host>:<db_port>/<db_name>
SECRET_KEY=<openssl_rand_hex_32>
```

## Resources 📚
List of resources used to carry out the project to date.

### Docs 📝
- [FastAPI](https://fastapi.tiangolo.com/es/) - `v. 0.70.0`
- [SQLAlchemy](https://docs.sqlalchemy.org/en/14/) - `v. 1.4.28`
- [S3 - Boto3](https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/s3.html#S3.Client.copy_object) - `1.20.26`
- [S3 - Digitalocean](https://docs.digitalocean.com/products/spaces/resources/s3-sdk-examples/) - `v. 0.5.0`
- [Swagger](https://swagger.io/docs/specification/about/) - `v. 3.0`
- [REST Client](https://marketplace.visualstudio.com/items?itemName=humao.rest-client#usage) - `v. 0.24.6`

### Articles and videos 💿
- [Building a CRUD APP with FastAPI and MySQL](https://blog.balasundar.com/building-a-crud-app-with-fastapi-and-mysql) - `Balasundar (2021)`
- [FastAPI por parte de su creador](https://www.youtube.com/watch?v=iOZ28g2fe-U) - `Software Guru (2020)`
- [FastAPI & SQLAlchemy RESTAPI CRUD](https://www.youtube.com/watch?v=6eVj33l5e9M) - `Fazt Code (2021)`
- [DigitalOcean Spaces & Nodejs (con Mongodb)](https://youtu.be/QLwJn_F84Sg?t=1823) - `Fazt Code (2020)`
- [Testing FastAPI Endpoints](https://medium.com/fastapi-tutorials/testing-fastapi-endpoints-f7e78f09b7b6) - `Ethan Cerami (2021)`

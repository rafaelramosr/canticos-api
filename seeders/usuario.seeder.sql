INSERT INTO `usuario` VALUES(
    1,
    'superadmin',
    'superadmin',
    'superadmin@choquisong.com',
    '$2b$12$HolSNdPVl5LFEkUEBjpZA.C31MkPXVkwL5pZAlDa4yVOM2QYRYJMC',
    'https://via.placeholder.com/300x300',
    1
);
INSERT INTO `usuario` VALUES(
    2,
    'admin',
    'admin',
    'admin@choquisong.com',
    '$2b$12$vacOtyseeU/itS7JzmGG4.dHbdJ4XMPqnyCd4rUPKz28H/ClQ7nqy',
    'https://via.placeholder.com/300x300',
    2
);
INSERT INTO `usuario` VALUES(
    3,
    'author',
    'author',
    'author@choquisong.com',
    '$2b$12$s59CSkkTGhgGLgxdUJVyzOS4or18f6SbQy0gmHcn/qy2a2WvZwmeO',
    'https://via.placeholder.com/300x300',
    3
);
INSERT INTO `usuario` VALUES(
    4,
    'user',
    'user',
    'user@choquisong.com',
    '$2b$12$4jKgyKqsyaVeLUqKChhWxu8eNpo/88UIGUuVfG.3jtdX7TaWup9uG',
    'https://via.placeholder.com/300x300',
    4
);

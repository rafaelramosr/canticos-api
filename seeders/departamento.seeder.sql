INSERT INTO `departamento` VALUES(1, 'Amazonas');
INSERT INTO `departamento` VALUES(2, 'Antioquía');
INSERT INTO `departamento` VALUES(3, 'Arauca');
INSERT INTO `departamento` VALUES(4, 'Atlántico');
INSERT INTO `departamento` VALUES(5, 'Bolívar');
INSERT INTO `departamento` VALUES(6, 'Boyacá');
INSERT INTO `departamento` VALUES(7, 'Caldas');
INSERT INTO `departamento` VALUES(8, 'Caquetá');
INSERT INTO `departamento` VALUES(9, 'Casanare');
INSERT INTO `departamento` VALUES(10, 'Cauca');
INSERT INTO `departamento` VALUES(11, 'Cesar');
INSERT INTO `departamento` VALUES(12, 'Chocó');
INSERT INTO `departamento` VALUES(13, 'Córdoba');
INSERT INTO `departamento` VALUES(14, 'Cundinamarca');
INSERT INTO `departamento` VALUES(15, 'Guainía');
INSERT INTO `departamento` VALUES(16, 'Guaviare');
INSERT INTO `departamento` VALUES(17, 'Huila');
INSERT INTO `departamento` VALUES(18, 'La Guajira');
INSERT INTO `departamento` VALUES(19, 'Magdalena');
INSERT INTO `departamento` VALUES(20, 'Meta');
INSERT INTO `departamento` VALUES(21, 'Nariño');
INSERT INTO `departamento` VALUES(22, 'Norte de Santander');
INSERT INTO `departamento` VALUES(23, 'Putumayo');
INSERT INTO `departamento` VALUES(24, 'Quindío');
INSERT INTO `departamento` VALUES(25, 'Risaralda');
INSERT INTO `departamento` VALUES(26, 'San Andrés y Providencia');
INSERT INTO `departamento` VALUES(27, 'Santander');
INSERT INTO `departamento` VALUES(28, 'Sucre');
INSERT INTO `departamento` VALUES(29, 'Tolima');
INSERT INTO `departamento` VALUES(30, 'Valle del Cauca');
INSERT INTO `departamento` VALUES(31, 'Vaupés');
INSERT INTO `departamento` VALUES(32, 'Vichada');

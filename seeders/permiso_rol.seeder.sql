--
-- Superuser
--

-- canciones
INSERT INTO `permiso_rol` VALUES(1, 1, 1);
INSERT INTO `permiso_rol` VALUES(2, 1, 2);
INSERT INTO `permiso_rol` VALUES(3, 1, 3);
INSERT INTO `permiso_rol` VALUES(4, 1, 4);
-- categorias
INSERT INTO `permiso_rol` VALUES(5, 1, 5);
INSERT INTO `permiso_rol` VALUES(6, 1, 6);
INSERT INTO `permiso_rol` VALUES(7, 1, 7);
INSERT INTO `permiso_rol` VALUES(8, 1, 8);
-- departamentos
INSERT INTO `permiso_rol` VALUES(9, 1, 9);
INSERT INTO `permiso_rol` VALUES(10, 1, 10);
INSERT INTO `permiso_rol` VALUES(11, 1, 11);
INSERT INTO `permiso_rol` VALUES(12, 1, 12);
-- municipios
INSERT INTO `permiso_rol` VALUES(13, 1, 13);
INSERT INTO `permiso_rol` VALUES(14, 1, 14);
INSERT INTO `permiso_rol` VALUES(15, 1, 15);
INSERT INTO `permiso_rol` VALUES(16, 1, 16);
-- permiso_rol
INSERT INTO `permiso_rol` VALUES(17, 1, 17);
INSERT INTO `permiso_rol` VALUES(18, 1, 18);
INSERT INTO `permiso_rol` VALUES(19, 1, 19);
INSERT INTO `permiso_rol` VALUES(20, 1, 20);
-- permisos
INSERT INTO `permiso_rol` VALUES(21, 1, 21);
INSERT INTO `permiso_rol` VALUES(22, 1, 22);
INSERT INTO `permiso_rol` VALUES(23, 1, 23);
INSERT INTO `permiso_rol` VALUES(24, 1, 24);
-- roles
INSERT INTO `permiso_rol` VALUES(25, 1, 25);
INSERT INTO `permiso_rol` VALUES(26, 1, 26);
INSERT INTO `permiso_rol` VALUES(27, 1, 27);
INSERT INTO `permiso_rol` VALUES(28, 1, 28);
-- usuarios
INSERT INTO `permiso_rol` VALUES(29, 1, 29);
INSERT INTO `permiso_rol` VALUES(30, 1, 30);
INSERT INTO `permiso_rol` VALUES(31, 1, 31);
INSERT INTO `permiso_rol` VALUES(32, 1, 32);
-- accion_bitacora
INSERT INTO `permiso_rol` VALUES(33, 1, 33);
INSERT INTO `permiso_rol` VALUES(34, 1, 34);
INSERT INTO `permiso_rol` VALUES(35, 1, 35);
INSERT INTO `permiso_rol` VALUES(36, 1, 36);
-- bitacora
INSERT INTO `permiso_rol` VALUES(37, 1, 37);
INSERT INTO `permiso_rol` VALUES(38, 1, 38);
INSERT INTO `permiso_rol` VALUES(39, 1, 39);
INSERT INTO `permiso_rol` VALUES(40, 1, 40);
-- logs
INSERT INTO `permiso_rol` VALUES(41, 1, 41);
INSERT INTO `permiso_rol` VALUES(42, 1, 42);
INSERT INTO `permiso_rol` VALUES(43, 1, 43);
INSERT INTO `permiso_rol` VALUES(44, 1, 44);

--
-- Admin
--

-- canciones
INSERT INTO `permiso_rol` VALUES(45, 2, 1);
INSERT INTO `permiso_rol` VALUES(46, 2, 2);
INSERT INTO `permiso_rol` VALUES(47, 2, 3);
INSERT INTO `permiso_rol` VALUES(48, 2, 4);
-- categorias
INSERT INTO `permiso_rol` VALUES(49, 2, 5);
INSERT INTO `permiso_rol` VALUES(50, 2, 6);
INSERT INTO `permiso_rol` VALUES(51, 2, 7);
INSERT INTO `permiso_rol` VALUES(52, 2, 8);
-- departamentos
INSERT INTO `permiso_rol` VALUES(53, 2, 9);
INSERT INTO `permiso_rol` VALUES(54, 2, 10);
INSERT INTO `permiso_rol` VALUES(55, 2, 11);
INSERT INTO `permiso_rol` VALUES(56, 2, 12);
-- municipios
INSERT INTO `permiso_rol` VALUES(57, 2, 13);
INSERT INTO `permiso_rol` VALUES(58, 2, 14);
INSERT INTO `permiso_rol` VALUES(59, 2, 15);
INSERT INTO `permiso_rol` VALUES(60, 2, 16);
-- permiso_rol
INSERT INTO `permiso_rol` VALUES(61, 2, 17);
INSERT INTO `permiso_rol` VALUES(62, 2, 18);
INSERT INTO `permiso_rol` VALUES(63, 2, 19);
INSERT INTO `permiso_rol` VALUES(64, 2, 20);
-- permisos
INSERT INTO `permiso_rol` VALUES(65, 2, 21);
INSERT INTO `permiso_rol` VALUES(66, 2, 22);
INSERT INTO `permiso_rol` VALUES(67, 2, 23);
INSERT INTO `permiso_rol` VALUES(68, 2, 24);
-- roles
INSERT INTO `permiso_rol` VALUES(69, 2, 25);
INSERT INTO `permiso_rol` VALUES(70, 2, 26);
INSERT INTO `permiso_rol` VALUES(71, 2, 27);
INSERT INTO `permiso_rol` VALUES(72, 2, 28);
-- usuarios
INSERT INTO `permiso_rol` VALUES(73, 2, 29);
INSERT INTO `permiso_rol` VALUES(74, 2, 30);
INSERT INTO `permiso_rol` VALUES(75, 2, 31);
INSERT INTO `permiso_rol` VALUES(76, 2, 32);

--
-- Autor
--

-- canciones
INSERT INTO `permiso_rol` VALUES(77, 3, 1);
INSERT INTO `permiso_rol` VALUES(78, 3, 2);
INSERT INTO `permiso_rol` VALUES(79, 3, 3);
INSERT INTO `permiso_rol` VALUES(80, 3, 4);
-- categorias
INSERT INTO `permiso_rol` VALUES(81, 3, 5);
INSERT INTO `permiso_rol` VALUES(82, 3, 6);
INSERT INTO `permiso_rol` VALUES(83, 3, 7);
INSERT INTO `permiso_rol` VALUES(84, 3, 8);
-- departamentos
INSERT INTO `permiso_rol` VALUES(85, 3, 25);
INSERT INTO `permiso_rol` VALUES(86, 3, 26);
INSERT INTO `permiso_rol` VALUES(87, 3, 27);
INSERT INTO `permiso_rol` VALUES(88, 3, 28);
-- municipios
INSERT INTO `permiso_rol` VALUES(89, 3, 29);
INSERT INTO `permiso_rol` VALUES(90, 3, 30);
INSERT INTO `permiso_rol` VALUES(91, 3, 31);
INSERT INTO `permiso_rol` VALUES(92, 3, 32);

--
-- Usuario
--

-- canciones
INSERT INTO `permiso_rol` VALUES(93, 4, 1);
INSERT INTO `permiso_rol` VALUES(94, 4, 2);
INSERT INTO `permiso_rol` VALUES(95, 4, 3);
INSERT INTO `permiso_rol` VALUES(96, 4, 4);
-- categorias
INSERT INTO `permiso_rol` VALUES(97, 4, 5);
INSERT INTO `permiso_rol` VALUES(98, 4, 6);
INSERT INTO `permiso_rol` VALUES(99, 4, 7);
INSERT INTO `permiso_rol` VALUES(100, 4, 8);
-- departamentos
INSERT INTO `permiso_rol` VALUES(101, 4, 25);
INSERT INTO `permiso_rol` VALUES(102, 4, 26);
INSERT INTO `permiso_rol` VALUES(103, 4, 27);
INSERT INTO `permiso_rol` VALUES(104, 4, 28);
-- municipios
INSERT INTO `permiso_rol` VALUES(105, 4, 29);
INSERT INTO `permiso_rol` VALUES(106, 4, 30);
INSERT INTO `permiso_rol` VALUES(107, 4, 31);
INSERT INTO `permiso_rol` VALUES(108, 4, 32);

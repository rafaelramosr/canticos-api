INSERT INTO `rol` VALUES(1, 'superadmin', 'Permisos de admin y acceso a los logs');
INSERT INTO `rol` VALUES(2, 'admin', 'Permisos de autor y control de usuarios');
INSERT INTO `rol` VALUES(3, 'autor', 'Permisos de usuario y creador de cantos');
INSERT INTO `rol` VALUES(4, 'usuario', 'Permisos básicos de lectura');

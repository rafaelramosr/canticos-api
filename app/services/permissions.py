from sqlalchemy.orm import Session
from typing import List

from ..api.permiso_rol import models, schemas


def get_permissions(rol_id: int, session: Session) -> List[str]:
    role_permissions: List[schemas.PermissionRolList] = (
        session.query(models.PermissionRolModel)
        .filter(models.PermissionRolModel.rol_id == rol_id)
        .all()
    )

    permissions = list(
        map((lambda permission: permission.permiso.nombre), role_permissions)
    )

    return permissions

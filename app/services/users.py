from sqlalchemy.orm import Session

from app.api.usuarios import models, schemas
from app.api.login import exceptions
from ..helpers.hashing import verify_password


def authenticate_user(username: str, password: str, session: Session) -> schemas.User:
    user_data = (
        session.query(models.UserModel)
        .filter(models.UserModel.email == username)
        .first()
    )

    if user_data is None:
        raise exceptions.UserNotFoundError

    if not verify_password(password, user_data.password):
        raise exceptions.UserNotFoundError

    return schemas.User(**user_data.__dict__)

from fastapi import FastAPI, Request
from fastapi.middleware.cors import CORSMiddleware
from fastapi.responses import JSONResponse
from fastapi.exceptions import RequestValidationError
from fastapi.exception_handlers import (
    http_exception_handler,
    request_validation_exception_handler,
)
from starlette.middleware import Middleware
from starlette.exceptions import HTTPException as StarletteHTTPException

from .routers import routers
from .helpers.set_log_data import set_log_data

middleware = [
    Middleware(
        CORSMiddleware,
        allow_origins=["*"],
        allow_credentials=True,
        allow_methods=["*"],
        allow_headers=["*"],
    )
]

app = FastAPI(middleware=middleware)

app.include_router(routers.router)


@app.exception_handler(StarletteHTTPException)
async def custom_http_exception_handler(request: Request, exc: StarletteHTTPException):
    response: JSONResponse = await http_exception_handler(request, exc)
    await set_log_data(str(request.url), repr(exc), response.status_code)
    return response


@app.exception_handler(RequestValidationError)
async def validation_exception_handler(request: Request, exc: RequestValidationError):
    response: JSONResponse = await request_validation_exception_handler(request, exc)
    await set_log_data(str(request.url), repr(exc), response.status_code)
    return response


@app.get("/")
async def root():
    return {"message": "Welcome to ChoquiSong API!"}

class TokenException(Exception):
    ...


class ExpiredTokenError(TokenException):
    def __init__(self):
        self.status_code = 401
        self.detail = "The access token provided is expired"


class InvalidCredentialsError(TokenException):
    def __init__(self):
        self.status_code = 401
        self.detail = "Could not validate credentials"

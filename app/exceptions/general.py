class GeneralException(Exception):
    ...


class NotFoundError(GeneralException):
    def __init__(self):
        self.status_code = 404
        self.detail = "Resource Not Found"


class AlreadyExistError(GeneralException):
    def __init__(self):
        self.status_code = 409
        self.detail = "Already Exists"

class S3Exception(Exception):
    ...


class S3ServiceUnavailable(S3Exception):
    def __init__(self):
        self.status_code = 500
        self.detail = "Can't access current service"

from functools import lru_cache
from pydantic import BaseSettings


class Settings(BaseSettings):
    action_binnacle_id: dict = {
        "DELETE": 1,
        "POST": 2,
        "PUT": 3,
    }
    algorithm_token: str = "HS256"
    aws_access_key_id: str
    aws_secret_access_key: str
    binnacle: dict = {
        "action_type": "",
        "ip_address": "",
        "new_data": {},
        "old_data": {},
        "table": "",
        "user_email": "",
        "user_id": None,
    }
    deafult_author_rol: int = 3
    deafult_user_rol: int = 4
    isLoggin: bool = False
    log: dict = {
        "current_url": "",
        "error_content": "",
        "status_code": "",
    }
    s3_bucket_name: str = "choquisong"
    s3_region_name: str = "sfo3"
    s3_endpoint_url: str ="https://sfo3.digitaloceanspaces.com"
    secret_key: str
    sql_database_url: str
    token_expire_minutes: int = 60
    type_permissions: dict = {
        "DELETE": "eliminar",
        "GET": "leer",
        "POST": "escribir",
        "PUT": "modificar",
    }

    class Config:
        env_file = ".env"


@lru_cache()
def get_settings():
    return Settings()


settings = get_settings()

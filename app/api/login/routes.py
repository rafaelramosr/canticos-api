from fastapi import APIRouter, Depends

from . import controllers


r_login = APIRouter(
    prefix="/login",
    tags=["login"],
    responses={404: {"description": "Not found"}},
)


@r_login.post("/")
async def login(
    user_data: dict = Depends(controllers.login),
):
    return user_data

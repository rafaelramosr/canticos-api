from fastapi import Depends, HTTPException
from fastapi.security import OAuth2PasswordRequestForm
from sqlalchemy.orm import Session

from ...db.config import get_db
from ...services.users import authenticate_user
from ...services.permissions import get_permissions
from ...helpers.token import create_access_token
from .exceptions import LoginException


async def login(
    form_data: OAuth2PasswordRequestForm = Depends(),
    session: Session = Depends(get_db),
):
    try:
        user_data = authenticate_user(form_data.username, form_data.password, session)
        permissions = get_permissions(user_data.rol_id, session)
        access_token = create_access_token(user_data.email, permissions)
        return {"access_token": access_token, "token_type": "bearer", "data": user_data}
    except LoginException as cie:
        raise HTTPException(**cie.__dict__)

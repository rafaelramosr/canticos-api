from sqlalchemy import Column, Integer, String
from sqlalchemy.orm import relationship

from ...db.config import Base


class RolModel(Base):
    __tablename__ = "rol"

    id = Column(Integer, primary_key=True, index=True)
    nombre = Column(String(50), nullable=False)
    descripcion = Column(String(250), nullable=True)

    meta_info = relationship("PermissionRolModel", back_populates="rol")

from pydantic import BaseModel
from typing import Optional


class CreateAndUpdateRol(BaseModel):
    nombre: str
    descripcion: Optional[str] = None


class Rol(CreateAndUpdateRol):
    id: int

    class Config:
        orm_mode = True

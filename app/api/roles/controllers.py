from fastapi import Depends, HTTPException
from sqlalchemy.orm import Session
from typing import List

from ...db.config import get_db
from ...helpers.set_log_data import set_binnacle_data
from .schemas import Rol, CreateAndUpdateRol
from .ops import get_all, get_one, insert_one, remove_one, update_one
from ...exceptions.general import GeneralException


async def create_rol(
    rol_data: CreateAndUpdateRol, session: Session = Depends(get_db)
) -> Rol:
    try:
        new_rol = insert_one(rol_data, session)
        await set_binnacle_data(Rol(**new_rol.__dict__).dict())
        return new_rol
    except GeneralException as cie:
        raise HTTPException(**cie.__dict__)


async def delete_rol(rol_id: int, session: Session = Depends(get_db)) -> Rol:
    try:
        rol_removed = remove_one(rol_id, session)
        await set_binnacle_data({}, Rol(**rol_removed.__dict__).dict())
        return rol_removed
    except GeneralException as cie:
        raise HTTPException(**cie.__dict__)


async def get_all_roles(session: Session = Depends(get_db)) -> List[Rol]:
    return get_all(session)


async def get_one_rol(rol_id: int, session: Session = Depends(get_db)) -> Rol:
    try:
        rol_data = get_one(rol_id, session)
        return rol_data
    except GeneralException as cie:
        raise HTTPException(**cie.__dict__)


async def update_rol(
    rol_data: CreateAndUpdateRol,
    rol_id: int,
    session: Session = Depends(get_db),
) -> Rol:
    try:
        rol_updated = update_one(rol_data, rol_id, session)
        await set_binnacle_data(Rol(**rol_updated.__dict__).dict())
        return rol_updated
    except GeneralException as cie:
        raise HTTPException(**cie.__dict__)

from fastapi import APIRouter, Depends
from typing import List

from ...helpers.token import require_token
from .schemas import Rol
from . import controllers


r_roles = APIRouter(
    prefix="/roles",
    tags=["roles"],
    dependencies=[Depends(require_token)],
    responses={404: {"description": "Not found"}},
)


@r_roles.delete("/{rol_id}")
async def delete_rol(
    rol_data: Rol = Depends(controllers.delete_rol),
):
    return rol_data


@r_roles.get("/")
async def get_all_roles(
    roles: List[Rol] = Depends(controllers.get_all_roles),
):
    return roles


@r_roles.get("/{rol_id}")
async def get_one_rol(
    rol: Rol = Depends(controllers.get_one_rol),
):
    return rol


@r_roles.post("/")
async def create_rol(
    rol_data: Rol = Depends(controllers.create_rol),
):
    return rol_data


@r_roles.put("/{rol_id}")
async def update_rol(
    rol_data: Rol = Depends(controllers.update_rol),
):
    return rol_data

from sqlalchemy.orm import Session
from typing import List

from app.config import settings
from .models import RolModel
from .schemas import CreateAndUpdateRol, Rol
from ...exceptions.general import AlreadyExistError, NotFoundError


def get_all(session: Session) -> List[Rol]:
    return session.query(RolModel).all()


def get_one(id: int, session: Session) -> Rol:
    rol_data = session.query(RolModel).get(id)

    if rol_data is None:
        raise NotFoundError

    return rol_data


def insert_one(rol_data: CreateAndUpdateRol, session: Session) -> Rol:
    rol_exists = (
        session.query(RolModel).filter(RolModel.nombre == rol_data.nombre).first()
    )
    if rol_exists is not None:
        raise AlreadyExistError

    new_rol = RolModel(**rol_data.dict())
    session.add(new_rol)
    session.commit()
    session.refresh(new_rol)
    return new_rol


def remove_one(id: int, session: Session) -> Rol:
    rol_removed = get_one(id, session)

    if rol_removed is None:
        raise NotFoundError

    session.delete(rol_removed)
    session.commit()

    return rol_removed


def update_one(rol_data: CreateAndUpdateRol, id: int, session: Session) -> Rol:
    rol_updated = get_one(id, session)
    settings.binnacle["old_data"] = Rol(**rol_updated.__dict__).dict()

    if rol_updated is None:
        raise NotFoundError

    rol_updated.nombre = rol_data.nombre
    rol_updated.descripcion = rol_data.descripcion

    session.commit()
    session.refresh(rol_updated)

    return rol_updated

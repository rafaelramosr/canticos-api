from fastapi import Depends, File, HTTPException, UploadFile
from sqlalchemy.orm import Session
from typing import List

from ...db.config import get_db
from ...helpers.set_log_data import set_binnacle_data
from ...helpers.file_storage import remove_file, update_file, upload_file
from ...exceptions.general import GeneralException, NotFoundError
from ...exceptions.s3 import S3Exception
from .schemas import CreateAndUpdateSong, Song, SongResquestForm, SongsWithDetails
from . import ops


async def create_song(
    audio: UploadFile = File(...),
    song_data: SongResquestForm = Depends(),
    session: Session = Depends(get_db),
) -> Song:
    try:
        s3_key = f"{song_data.autor}:{song_data.nombre}"
        song_url = upload_file(
            audio.file, s3_key, audio.content_type, song_data.categoria_id
        )

        new_song = CreateAndUpdateSong(**song_data.__dict__, url=song_url)
        created_song = ops.insert_one(new_song, session)
        await set_binnacle_data(Song(**created_song.__dict__).dict())
        return created_song
    except (GeneralException, S3Exception) as cie:
        raise HTTPException(**cie.__dict__)


async def get_all_songs(session: Session = Depends(get_db)) -> List[SongsWithDetails]:
    songs = ops.get_all(session)
    for song in songs:
        song.usuario
    return songs


async def get_one_song(song_id: int, session: Session = Depends(get_db)) -> Song:
    try:
        return ops.get_one(song_id, session)
    except GeneralException as cie:
        raise HTTPException(**cie.__dict__)


async def songs_by_author(
    author_id: int, session: Session = Depends(get_db)
) -> List[Song]:
    try:
        return ops.get_all_by_author(author_id, session)
    except GeneralException as cie:
        raise HTTPException(**cie.__dict__)


async def remove_song(song_id: int, session: Session = Depends(get_db)) -> Song:
    try:
        delete_song = ops.remove_one(song_id, session)
        s3_key = f"{delete_song.autor}:{delete_song.nombre}"
        remove_file(s3_key)
        await set_binnacle_data({}, Song(**delete_song.__dict__).dict())
        return delete_song
    except (GeneralException, S3Exception) as cie:
        raise HTTPException(**cie.__dict__)


async def update_song(
    song_data: SongResquestForm, song_id: int, session: Session = Depends(get_db)
) -> Song:
    try:
        current_song: Song = ops.get_one(song_id, session)

        if current_song is None:
            raise NotFoundError

        s3_new_key = f"{song_data.autor}:{song_data.nombre}"
        s3_old_key = f"{current_song.autor}:{current_song.nombre}"
        new_song_url = update_file(s3_old_key, s3_new_key, song_data.categoria_id)
        song_updated = ops.update_one(
            CreateAndUpdateSong(**song_data.__dict__, url=new_song_url),
            song_id,
            session,
        )
        await set_binnacle_data(Song(**song_updated.__dict__).dict())
        return song_updated
    except (GeneralException, S3Exception) as cie:
        raise HTTPException(**cie.__dict__)

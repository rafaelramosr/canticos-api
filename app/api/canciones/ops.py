from sqlalchemy.orm import Session
from typing import List

from app.config import settings
from .models import SongModel
from .schemas import CreateAndUpdateSong, Song, SongsWithDetails
from ...exceptions.general import AlreadyExistError, NotFoundError


def get_all(session: Session) -> List[SongsWithDetails]:
    return session.query(SongModel).all()


def get_one(id: int, session: Session) -> Song:
    song_data = session.query(SongModel).get(id)

    if song_data is None:
        raise NotFoundError

    return song_data


def get_all_by_author(author_id: int, session: Session) -> List[Song]:
    song_data = session.query(SongModel).filter_by(usuario_id=author_id).all()

    if song_data is None:
        raise NotFoundError

    return song_data


def insert_one(song_data: CreateAndUpdateSong, session: Session) -> Song:
    song_exists = (
        session.query(SongModel)
        .filter(SongModel.nombre == song_data.nombre)
        .filter(SongModel.autor == song_data.autor)
        .first()
    )

    if song_exists is not None:
        raise AlreadyExistError

    new_song = SongModel(**song_data.dict())
    session.add(new_song)
    session.commit()
    session.refresh(new_song)
    return new_song


def remove_one(id: int, session: Session) -> Song:
    song_removed = get_one(id, session)

    if song_removed is None:
        raise NotFoundError

    session.delete(song_removed)
    session.commit()

    return song_removed


def update_one(song_data: CreateAndUpdateSong, id: int, session: Session) -> Song:
    song_updated = get_one(id, session)
    settings.binnacle["old_data"] = Song(**song_updated.__dict__).dict()

    if (
        song_updated.nombre != song_data.nombre
        and song_updated.autor != song_data.autor
    ):
        song_exists = (
            session.query(SongModel)
            .filter(SongModel.nombre == song_data.nombre)
            .filter(SongModel.autor == song_data.autor)
            .first()
        )

        if song_exists and len(song_exists) > 0:
            raise AlreadyExistError

    song_updated.nombre = song_data.nombre
    song_updated.letra = song_data.letra
    song_updated.autor = song_data.autor
    song_updated.url = song_data.url
    song_updated.categoria_id = song_data.categoria_id
    song_updated.municipio_id = song_data.municipio_id
    song_updated.usuario_id = song_data.usuario_id

    session.commit()
    session.refresh(song_updated)

    return song_updated

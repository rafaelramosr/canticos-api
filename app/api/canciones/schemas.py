from pydantic import BaseModel

from app.helpers.formdata import form_body


@form_body
class SongResquestForm(BaseModel):
    nombre: str
    letra: str
    autor: str
    categoria_id: int
    municipio_id: int
    usuario_id: int


class UserSong(BaseModel):
    apellido: str
    email: str
    nombre: str
    rol_id: int
    url: str


class CreateAndUpdateSong(SongResquestForm):
    url: str = ""


class SongsWithDetails(CreateAndUpdateSong):
    id: int
    usuario: UserSong


class Song(CreateAndUpdateSong):
    id: int

    class Config:
        orm_mode = True

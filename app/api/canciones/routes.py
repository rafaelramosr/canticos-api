from fastapi import APIRouter, Depends
from typing import List

from ...helpers.token import require_token
from .schemas import CreateAndUpdateSong, Song
from . import controllers


r_songs = APIRouter(
    prefix="/canciones",
    tags=["canciones"],
    dependencies=[Depends(require_token)],
    responses={404: {"description": "Not found"}},
)


@r_songs.delete("/{song_id}")
async def remove_song(song: Song = Depends(controllers.remove_song)):
    return song


@r_songs.get("/")
async def get_all_songs(
    songs: List[Song] = Depends(controllers.get_all_songs),
):
    return songs


@r_songs.get("/{song_id}")
async def get_one_song(song: Song = Depends(controllers.get_one_song)):
    return song


@r_songs.get("/author/{author_id}")
async def songs_by_author(songs: List[Song] = Depends(controllers.songs_by_author)):
    return songs


@r_songs.post("/")
async def create_song(
    song_data: CreateAndUpdateSong = Depends(controllers.create_song),
):
    return song_data


@r_songs.put("/{song_id}")
async def update_song(
    song_data: CreateAndUpdateSong = Depends(controllers.update_song),
):
    return song_data

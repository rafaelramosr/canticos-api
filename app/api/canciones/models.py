from sqlalchemy import Column, ForeignKey, Integer, String
from sqlalchemy.dialects.mysql import MEDIUMTEXT
from sqlalchemy.orm import relationship

from ...db.config import Base


class SongModel(Base):
    __tablename__ = "cancion"

    id = Column(Integer, primary_key=True, index=True)
    nombre = Column(String(100), nullable=False)
    letra = Column(MEDIUMTEXT, nullable=False)
    autor = Column(String(100), nullable=False)
    url = Column(String(500), nullable=False)
    categoria_id = Column(Integer, ForeignKey("categoria.id"), nullable=False)
    municipio_id = Column(Integer, ForeignKey("municipio.id"), nullable=False)
    usuario_id = Column(Integer, ForeignKey("usuario.id"), nullable=False)

    usuario = relationship("UserModel", back_populates="meta_info")

from fastapi import Depends, HTTPException
from sqlalchemy.orm import Session
from typing import List

from ...db.config import get_db
from ...helpers.set_log_data import set_binnacle_data
from ...exceptions.general import GeneralException
from .schemas import PermissionRol, CreateAndUpdatePermissionRol, PermissionRolList
from .ops import get_all, get_one, insert_one, remove_one, update_one


async def create_permission(
    permission_rol_data: CreateAndUpdatePermissionRol,
    session: Session = Depends(get_db),
) -> PermissionRol:
    try:
        new_permission = insert_one(permission_rol_data, session)
        await set_binnacle_data(PermissionRol(**new_permission.__dict__).dict())
        return new_permission
    except GeneralException as cie:
        raise HTTPException(**cie.__dict__)


async def delete_permission(
    permission_rol_id: int, session: Session = Depends(get_db)
) -> PermissionRol:
    try:
        permission_rol_removed = remove_one(permission_rol_id, session)
        await set_binnacle_data(
            {}, PermissionRol(**permission_rol_removed.__dict__).dict()
        )
        return permission_rol_removed
    except GeneralException as cie:
        raise HTTPException(**cie.__dict__)


async def get_all_permissions(
    session: Session = Depends(get_db),
) -> List[PermissionRolList]:
    results = get_all(session)

    for result in results:
        result.permiso
        result.rol

    return results


async def get_one_permission(
    permission_rol_id: int, session: Session = Depends(get_db)
) -> PermissionRol:
    try:
        permission_rol_data = get_one(permission_rol_id, session)
        return permission_rol_data
    except GeneralException as cie:
        raise HTTPException(**cie.__dict__)


async def update_permission(
    permission_rol_data: CreateAndUpdatePermissionRol,
    permission_rol_id: int,
    session: Session = Depends(get_db),
) -> PermissionRol:
    try:
        permission_rol_updated = update_one(
            permission_rol_data, permission_rol_id, session
        )
        await set_binnacle_data(PermissionRol(**permission_rol_updated.__dict__).dict())
        return permission_rol_updated
    except GeneralException as cie:
        raise HTTPException(**cie.__dict__)

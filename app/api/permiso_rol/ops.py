from sqlalchemy.orm import Session
from typing import List

from app.config import settings
from ...exceptions.general import AlreadyExistError, NotFoundError
from .models import PermissionRolModel
from .schemas import PermissionRol, CreateAndUpdatePermissionRol, PermissionRolList


def get_all(session: Session) -> List[PermissionRolList]:
    return session.query(PermissionRolModel).all()


def get_one(id: int, session: Session) -> PermissionRol:
    permission_rol_data = session.query(PermissionRolModel).get(id)

    if permission_rol_data is None:
        raise NotFoundError

    return permission_rol_data


def insert_one(
    permission_rol_data: CreateAndUpdatePermissionRol, session: Session
) -> PermissionRol:
    permission_rol_exists = (
        session.query(PermissionRolModel)
        .filter(PermissionRolModel.permiso_id == permission_rol_data.permiso_id)
        .filter(PermissionRolModel.rol_id == permission_rol_data.rol_id)
        .first()
    )

    if permission_rol_exists is not None:
        raise AlreadyExistError

    new_permission = PermissionRolModel(**permission_rol_data.dict())
    session.add(new_permission)
    session.commit()
    session.refresh(new_permission)
    return new_permission


def remove_one(id: int, session: Session) -> PermissionRol:
    permission_rol_removed = get_one(id, session)

    if permission_rol_removed is None:
        raise NotFoundError

    session.delete(permission_rol_removed)
    session.commit()

    return permission_rol_removed


def update_one(
    permission_rol_data: CreateAndUpdatePermissionRol, id: int, session: Session
) -> PermissionRol:
    permission_rol_updated = get_one(id, session)
    settings.binnacle["old_data"] = PermissionRol(
        **permission_rol_updated.__dict__
    ).dict()

    if permission_rol_updated is None:
        raise NotFoundError

    permission_rol_updated.permiso_id = permission_rol_data.permiso_id
    permission_rol_updated.rol_id = permission_rol_data.rol_id

    session.commit()
    session.refresh(permission_rol_updated)

    return permission_rol_updated

from fastapi import APIRouter, Depends
from typing import List

from ...helpers.token import require_token
from .schemas import PermissionRol, PermissionRolList
from . import controllers


r_permission_rol = APIRouter(
    prefix="/permiso_rol",
    tags=["permiso_rol"],
    dependencies=[Depends(require_token)],
    responses={404: {"description": "Not found"}},
)


@r_permission_rol.delete("/{permission_rol_id}")
async def delete_permission(
    permission_rol_data: PermissionRol = Depends(controllers.delete_permission),
):
    return permission_rol_data


@r_permission_rol.get("/")
async def get_all_permissions(
    permissions: List[PermissionRolList] = Depends(controllers.get_all_permissions),
):
    return permissions


@r_permission_rol.get("/{permission_rol_id}")
async def get_one_permission(
    permission_rol: PermissionRol = Depends(controllers.get_one_permission),
):
    return permission_rol


@r_permission_rol.post("/")
async def create_permission(
    permission_rol_data: PermissionRol = Depends(controllers.create_permission),
):
    return permission_rol_data


@r_permission_rol.put("/{permission_rol_id}")
async def update_permission(
    permission_rol_data: PermissionRol = Depends(controllers.update_permission),
):
    return permission_rol_data

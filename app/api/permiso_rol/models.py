from sqlalchemy import Column, ForeignKey, Integer
from sqlalchemy.orm import relationship

from ...db.config import Base


class PermissionRolModel(Base):
    __tablename__ = "permiso_rol"

    id = Column(Integer, primary_key=True, index=True)
    rol_id = Column(Integer, ForeignKey("rol.id"), nullable=False)
    permiso_id = Column(Integer, ForeignKey("permiso.id"), nullable=False)

    rol = relationship("RolModel", back_populates="meta_info")
    permiso = relationship("PermissionModel", back_populates="meta_info")

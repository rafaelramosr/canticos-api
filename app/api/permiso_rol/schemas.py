from pydantic import BaseModel


class Permission(BaseModel):
    id: int
    nombre: str


class Rol(Permission):
    descripcion: str


class CreateAndUpdatePermissionRol(BaseModel):
    permiso_id: int
    rol_id: int


class PermissionRol(CreateAndUpdatePermissionRol):
    id: int

    class Config:
        orm_mode = True


class PermissionRolList(PermissionRol):
    permiso: Permission
    rol: Rol

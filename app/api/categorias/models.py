from sqlalchemy import Column, Integer, String

from ...db.config import Base


class CategoryModel(Base):
    __tablename__ = "categoria"

    id = Column(Integer, primary_key=True, index=True)
    nombre = Column(String(100), nullable=False)

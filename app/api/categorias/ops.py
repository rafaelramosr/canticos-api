from sqlalchemy.orm import Session
from typing import List

from app.config import settings
from ...exceptions.general import AlreadyExistError, NotFoundError
from .models import CategoryModel
from .schemas import Category, CreateAndUpdateCategory


def get_all(session: Session) -> List[Category]:
    return session.query(CategoryModel).all()


def get_one(id: int, session: Session) -> Category:
    category_data = session.query(CategoryModel).get(id)

    if category_data is None:
        raise NotFoundError

    return category_data


def insert_one(category_data: CreateAndUpdateCategory, session: Session) -> Category:
    category_exists = (
        session.query(CategoryModel)
        .filter(CategoryModel.nombre == category_data.nombre)
        .first()
    )
    if category_exists is not None:
        raise AlreadyExistError

    new_category = CategoryModel(**category_data.dict())
    session.add(new_category)
    session.commit()
    session.refresh(new_category)
    return new_category


def remove_one(id: int, session: Session) -> Category:
    category_removed = get_one(id, session)

    if category_removed is None:
        raise NotFoundError

    session.delete(category_removed)
    session.commit()

    return category_removed


def update_one(
    category_data: CreateAndUpdateCategory, id: int, session: Session
) -> Category:
    category_updated = get_one(id, session)
    settings.binnacle["old_data"] = Category(**category_updated.__dict__).dict()

    if category_updated is None:
        raise NotFoundError

    category_updated.nombre = category_data.nombre

    session.commit()
    session.refresh(category_updated)

    return category_updated

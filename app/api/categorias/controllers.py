from fastapi import Depends, HTTPException
from sqlalchemy.orm import Session
from typing import List

from ...db.config import get_db
from ...helpers.set_log_data import set_binnacle_data
from ...exceptions.general import GeneralException
from .schemas import Category, CreateAndUpdateCategory
from .ops import get_all, get_one, insert_one, remove_one, update_one


async def create_category(
    category_data: CreateAndUpdateCategory, session: Session = Depends(get_db)
) -> Category:
    try:
        new_category = insert_one(category_data, session)
        await set_binnacle_data(Category(**new_category.__dict__).dict())
        return new_category
    except GeneralException as cie:
        raise HTTPException(**cie.__dict__)


async def delete_category(
    category_id: int, session: Session = Depends(get_db)
) -> Category:
    try:
        category_removed = remove_one(category_id, session)
        await set_binnacle_data({}, Category(**category_removed.__dict__).dict())
        return category_removed
    except GeneralException as cie:
        raise HTTPException(**cie.__dict__)


async def get_all_categories(session: Session = Depends(get_db)) -> List[Category]:
    return get_all(session)


async def get_one_category(
    category_id: int, session: Session = Depends(get_db)
) -> Category:
    try:
        category_data = get_one(category_id, session)
        return category_data
    except GeneralException as cie:
        raise HTTPException(**cie.__dict__)


async def update_category(
    category_data: CreateAndUpdateCategory,
    category_id: int,
    session: Session = Depends(get_db),
) -> Category:
    try:
        category_updated = update_one(category_data, category_id, session)
        await set_binnacle_data(Category(**category_updated.__dict__).dict())
        return category_updated
    except GeneralException as cie:
        raise HTTPException(**cie.__dict__)

from pydantic import BaseModel


class CreateAndUpdateCategory(BaseModel):
    nombre: str


class Category(CreateAndUpdateCategory):
    id: int

    class Config:
        orm_mode = True

from fastapi import APIRouter, Depends
from typing import List

from ...helpers.token import require_token
from .schemas import Category
from . import controllers


r_categories = APIRouter(
    prefix="/categorias",
    tags=["categorias"],
    dependencies=[Depends(require_token)],
    responses={404: {"description": "Not found"}},
)


@r_categories.delete("/{category_id}")
async def delete_category(
    category_data: Category = Depends(controllers.delete_category),
):
    return category_data


@r_categories.get("/")
async def get_all_categories(
    categories: List[Category] = Depends(controllers.get_all_categories),
):
    return categories


@r_categories.get("/{category_id}")
async def get_one_category(
    category: Category = Depends(controllers.get_one_category),
):
    return category


@r_categories.post("/")
async def create_category(
    category_data: Category = Depends(controllers.create_category),
):
    return category_data


@r_categories.put("/{category_id}")
async def update_category(
    category_data: Category = Depends(controllers.update_category),
):
    return category_data

from pydantic import BaseModel


class Municipality(BaseModel):
    id: int
    nombre: str
    departamento_id: int

    class Config:
        orm_mode = True

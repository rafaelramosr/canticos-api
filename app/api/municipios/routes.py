from fastapi import APIRouter, Depends
from typing import List

from ...helpers.token import require_token
from .schemas import Municipality
from . import controllers


r_municipalities = APIRouter(
    prefix="/municipios",
    tags=["municipios"],
    dependencies=[Depends(require_token)],
    responses={404: {"description": "Not found"}},
)


@r_municipalities.get("/{departamento_id}")
async def get_by_department(
    municipalities: List[Municipality] = Depends(controllers.get_by_department),
):
    return municipalities


@r_municipalities.get("/")
async def get_all_municipalities(
    municipalities: List[Municipality] = Depends(controllers.get_all_municipalities),
):
    return municipalities

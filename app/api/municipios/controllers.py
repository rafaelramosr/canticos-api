from fastapi import Depends, HTTPException
from sqlalchemy.orm import Session
from typing import List

from ...db.config import get_db
from . import schemas, ops
from ...exceptions.general import GeneralException


async def get_all_municipalities(
    session: Session = Depends(get_db),
) -> List[schemas.Municipality]:
    return ops.get_all(session)


async def get_by_department(
    departamento_id: int, session: Session = Depends(get_db)
) -> List[schemas.Municipality]:
    try:
        municipality_data = ops.get_by_id(departamento_id, session)
        return municipality_data
    except GeneralException as cie:
        raise HTTPException(**cie.__dict__)

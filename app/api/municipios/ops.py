from sqlalchemy.orm import Session
from typing import List

from . import models, schemas
from ...exceptions.general import NotFoundError


def get_all(session: Session) -> List[schemas.Municipality]:
    return session.query(models.MunicipalityModel).all()


def get_by_id(departamento_id: int, session: Session) -> List[schemas.Municipality]:
    municipalities = (
        session.query(models.MunicipalityModel)
        .filter(models.MunicipalityModel.departamento_id == departamento_id)
        .all()
    )

    if len(municipalities) == 0:
        raise NotFoundError

    return municipalities

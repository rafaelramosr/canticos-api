from sqlalchemy import Column, ForeignKey, Integer, String

from ...db.config import Base


class MunicipalityModel(Base):
    __tablename__ = "municipio"

    id = Column(Integer, primary_key=True, index=True)
    nombre = Column(String(100), nullable=False)
    departamento_id = Column(Integer, ForeignKey("departamento.id"), nullable=False)

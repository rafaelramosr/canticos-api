from fastapi import Depends, HTTPException
from sqlalchemy.orm import Session
from typing import List

from ...db.config import get_db
from ...exceptions.general import GeneralException
from .schemas import AccionBitacoras, CreateAndUpdateAccionBitacora
from .ops import get_all, insert_one


async def get_all_accion_bitacora(
    session: Session = Depends(get_db),
) -> List[AccionBitacoras]:
    return get_all(session)


async def create_accion_bitacora(
    accion_bitacora_data: CreateAndUpdateAccionBitacora,
    session: Session = Depends(get_db),
) -> AccionBitacoras:
    try:
        new_accion_bitacora = insert_one(accion_bitacora_data, session)
        return new_accion_bitacora
    except GeneralException as cie:
        raise HTTPException(**cie.__dict__)

from pydantic import BaseModel


class CreateAndUpdateAccionBitacora(BaseModel):
    nombre: str


class AccionBitacoras(CreateAndUpdateAccionBitacora):
    id: int

    class Config:
        orm_mode = True

from sqlalchemy import Column, Integer, String

from ...db.config import Base


class AccionBitacoraModel(Base):
    __tablename__ = "accion_bitacora"

    id = Column(Integer, primary_key=True, index=True)
    nombre = Column(String(100), nullable=False)

from fastapi import APIRouter, Depends
from typing import List

from ...helpers.token import require_token
from .schemas import AccionBitacoras
from . import controllers


r_accion_bitacoras = APIRouter(
    prefix="/accion_bitacora",
    tags=["accion_bitacora"],
    dependencies=[Depends(require_token)],
    responses={404: {"description": "Not found"}},
)


@r_accion_bitacoras.get("/")
async def get_all_accion_bitacora(
    accion_bitacoras: List[AccionBitacoras] = Depends(
        controllers.get_all_accion_bitacora
    ),
):
    return accion_bitacoras


@r_accion_bitacoras.post("/")
async def create_accion_bitacoras(
    accion_bitacoras_data: AccionBitacoras = Depends(
        controllers.create_accion_bitacora
    ),
):
    return accion_bitacoras_data

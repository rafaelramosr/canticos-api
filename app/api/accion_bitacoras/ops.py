from sqlalchemy.orm import Session
from typing import List

from ...exceptions.general import AlreadyExistError
from .models import AccionBitacoraModel
from .schemas import AccionBitacoras, CreateAndUpdateAccionBitacora


def get_all(session: Session) -> List[AccionBitacoras]:
    return session.query(AccionBitacoraModel).all()


def insert_one(
    accion_bitacora: CreateAndUpdateAccionBitacora, session: Session
) -> AccionBitacoras:
    accion_bitacora_exists = (
        session.query(AccionBitacoraModel)
        .filter(AccionBitacoraModel.nombre == accion_bitacora.nombre)
        .first()
    )

    if accion_bitacora_exists is not None:
        raise AlreadyExistError

    new_accion_bitacora = AccionBitacoraModel(**accion_bitacora.dict())
    session.add(new_accion_bitacora)
    session.commit()
    session.refresh(new_accion_bitacora)
    return new_accion_bitacora

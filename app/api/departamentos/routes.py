from fastapi import APIRouter, Depends
from typing import List

from ...helpers.token import require_token
from .schemas import Department
from . import controllers


r_departments = APIRouter(
    prefix="/departamentos",
    tags=["departamentos"],
    dependencies=[Depends(require_token)],
    responses={404: {"description": "Not found"}},
)


@r_departments.delete("/{department_id}")
async def delete_department(
    department_data: Department = Depends(controllers.delete_department),
):
    return department_data


@r_departments.get("/")
async def get_all_departments(
    departments: List[Department] = Depends(controllers.get_all_departments),
):
    return departments


@r_departments.get("/{department_id}")
async def get_one_department(
    department: Department = Depends(controllers.get_one_department),
):
    return department


@r_departments.post("/")
async def create_department(
    department_data: Department = Depends(controllers.create_department),
):
    return department_data


@r_departments.put("/{department_id}")
async def update_department(
    department_data: Department = Depends(controllers.update_department),
):
    return department_data

from sqlalchemy.orm import Session
from typing import List

from app.config import settings
from ...exceptions.general import AlreadyExistError, NotFoundError
from .models import DepartmentModel
from .schemas import Department, CreateAndUpdateDepartment


def get_all(session: Session) -> List[Department]:
    return session.query(DepartmentModel).all()


def get_one(id: int, session: Session) -> Department:
    department_data = session.query(DepartmentModel).get(id)

    if department_data is None:
        raise NotFoundError

    return department_data


def insert_one(
    department_data: CreateAndUpdateDepartment, session: Session
) -> Department:
    department_exists = (
        session.query(DepartmentModel)
        .filter(DepartmentModel.nombre == department_data.nombre)
        .first()
    )
    if department_exists is not None:
        raise AlreadyExistError

    new_department = DepartmentModel(**department_data.dict())
    session.add(new_department)
    session.commit()
    session.refresh(new_department)
    return new_department


def remove_one(id: int, session: Session) -> Department:
    department_removed = get_one(id, session)

    if department_removed is None:
        raise NotFoundError

    session.delete(department_removed)
    session.commit()

    return department_removed


def update_one(
    department_data: CreateAndUpdateDepartment, id: int, session: Session
) -> Department:
    department_updated = get_one(id, session)
    settings.binnacle["old_data"] = Department(**department_updated.__dict__).dict()

    if department_updated is None:
        raise NotFoundError

    department_updated.nombre = department_data.nombre

    session.commit()
    session.refresh(department_updated)

    return department_updated

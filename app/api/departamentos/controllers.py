from fastapi import Depends, HTTPException
from sqlalchemy.orm import Session
from typing import List

from ...db.config import get_db
from ...helpers.set_log_data import set_binnacle_data
from ...exceptions.general import GeneralException
from .schemas import Department, CreateAndUpdateDepartment
from .ops import get_all, get_one, insert_one, remove_one, update_one


async def create_department(
    department_data: CreateAndUpdateDepartment, session: Session = Depends(get_db)
) -> Department:
    try:
        new_department = insert_one(department_data, session)
        await set_binnacle_data(Department(**new_department.__dict__).dict())
        return new_department
    except GeneralException as cie:
        raise HTTPException(**cie.__dict__)


async def delete_department(
    department_id: int, session: Session = Depends(get_db)
) -> Department:
    try:
        department_removed = remove_one(department_id, session)
        await set_binnacle_data({}, Department(**department_removed.__dict__).dict())
        return department_removed
    except GeneralException as cie:
        raise HTTPException(**cie.__dict__)


async def get_all_departments(session: Session = Depends(get_db)) -> List[Department]:
    return get_all(session)


async def get_one_department(
    department_id: int, session: Session = Depends(get_db)
) -> Department:
    try:
        department_data = get_one(department_id, session)
        return department_data
    except GeneralException as cie:
        raise HTTPException(**cie.__dict__)


async def update_department(
    department_data: CreateAndUpdateDepartment,
    department_id: int,
    session: Session = Depends(get_db),
) -> Department:
    try:
        department_updated = update_one(department_data, department_id, session)
        await set_binnacle_data(Department(**department_updated.__dict__).dict())
        return department_updated
    except GeneralException as cie:
        raise HTTPException(**cie.__dict__)

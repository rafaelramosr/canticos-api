from pydantic import BaseModel


class CreateAndUpdateDepartment(BaseModel):
    nombre: str


class Department(CreateAndUpdateDepartment):
    id: int

    class Config:
        orm_mode = True

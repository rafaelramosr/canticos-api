from sqlalchemy import Column, Integer, String

from ...db.config import Base


class DepartmentModel(Base):
    __tablename__ = "departamento"

    id = Column(Integer, primary_key=True, index=True)
    nombre = Column(String(100), nullable=False)

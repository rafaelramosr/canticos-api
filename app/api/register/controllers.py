from fastapi import Depends, HTTPException
from sqlalchemy.orm import Session

from ...db.config import get_db
from ...services.permissions import get_permissions
from ...api.usuarios.schemas import CreateUser, User
from ...api.usuarios.controllers import create_user
from ...helpers.token import create_access_token
from .exceptions import LoginException


async def register_user(
    user_data: CreateUser,
    session: Session = Depends(get_db),
):
    try:
        new_user = await create_user(user_data, session)
        permissions = get_permissions(new_user.rol_id, session)
        access_token = create_access_token(user_data.email, permissions)
        return {
            "access_token": access_token,
            "token_type": "bearer",
            "data": User(**new_user.__dict__),
        }
    except LoginException as cie:
        raise HTTPException(**cie.__dict__)

from fastapi import APIRouter, Depends

from app.api.usuarios.schemas import CreateUser
from . import controllers


r_register = APIRouter(
    prefix="/register",
    tags=["register"],
    responses={404: {"description": "Not found"}},
)


@r_register.post("/")
async def register(user_data: CreateUser = Depends(controllers.register_user)):
    return user_data

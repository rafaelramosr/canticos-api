class LoginException(Exception):
    ...


class UserNotFoundError(LoginException):
    def __init__(self):
        self.status_code = 401
        self.detail = "Incorrect username or password"

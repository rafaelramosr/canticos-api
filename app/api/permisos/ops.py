from sqlalchemy.orm import Session
from typing import List

from app.config import settings
from .models import PermissionModel
from .schemas import Permission, CreateAndUpdatePermission
from ...exceptions.general import AlreadyExistError, NotFoundError


def get_all(session: Session) -> List[Permission]:
    return session.query(PermissionModel).all()


def get_one(id: int, session: Session) -> Permission:
    permission_data = session.query(PermissionModel).get(id)

    if permission_data is None:
        raise NotFoundError

    return permission_data


def insert_one(
    permission_data: CreateAndUpdatePermission, session: Session
) -> Permission:
    permission_exists = (
        session.query(PermissionModel)
        .filter(PermissionModel.nombre == permission_data.nombre)
        .first()
    )
    if permission_exists is not None:
        raise AlreadyExistError

    new_permission = PermissionModel(**permission_data.dict())
    session.add(new_permission)
    session.commit()
    session.refresh(new_permission)
    return new_permission


def remove_one(id: int, session: Session) -> Permission:
    permission_removed = get_one(id, session)

    if permission_removed is None:
        raise NotFoundError

    session.delete(permission_removed)
    session.commit()

    return permission_removed


def update_one(
    permission_data: CreateAndUpdatePermission, id: int, session: Session
) -> Permission:
    permission_updated = get_one(id, session)
    settings.binnacle["old_data"] = Permission(**permission_updated.__dict__).dict()

    if permission_updated is None:
        raise NotFoundError

    permission_updated.nombre = permission_data.nombre

    session.commit()
    session.refresh(permission_updated)

    return permission_updated

class PermissionException(Exception):
    ...


class PermissionNotAllowed(PermissionException):
    def __init__(self):
        self.status_code = 409
        self.detail = "Permission name does not match syntax"

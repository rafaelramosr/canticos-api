from pydantic import BaseModel


class CreateAndUpdatePermission(BaseModel):
    nombre: str


class Permission(CreateAndUpdatePermission):
    id: int

    class Config:
        orm_mode = True

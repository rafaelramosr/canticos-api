from fastapi import APIRouter, Depends
from typing import List

from ...helpers.token import require_token
from .schemas import Permission
from . import controllers


r_permissions = APIRouter(
    prefix="/permisos",
    tags=["permisos"],
    dependencies=[Depends(require_token)],
    responses={404: {"description": "Not found"}},
)


@r_permissions.delete("/{permission_id}")
async def delete_permission(
    permission_data: Permission = Depends(controllers.delete_permission),
):
    return permission_data


@r_permissions.get("/")
async def get_all_permissions(
    permissions: List[Permission] = Depends(controllers.get_all_permissions),
):
    return permissions


@r_permissions.get("/{permission_id}")
async def get_one_permission(
    permission: Permission = Depends(controllers.get_one_permission),
):
    return permission


@r_permissions.post("/")
async def create_permission(
    permission_data: Permission = Depends(controllers.create_permission),
):
    return permission_data


@r_permissions.put("/{permission_id}")
async def update_permission(
    permission_data: Permission = Depends(controllers.update_permission),
):
    return permission_data

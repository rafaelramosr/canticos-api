from fastapi import Depends, HTTPException
from sqlalchemy.orm import Session
from typing import List

from ...db.config import get_db
from ...helpers.set_log_data import set_binnacle_data
from ...exceptions.general import GeneralException
from .schemas import Permission, CreateAndUpdatePermission
from .ops import get_all, get_one, insert_one, remove_one, update_one
from .exceptions import PermissionException, PermissionNotAllowed


async def create_permission(
    permission_data: CreateAndUpdatePermission, session: Session = Depends(get_db)
) -> Permission:
    try:
        if permission_data.nombre.count(":") != 1:
            raise PermissionNotAllowed

        new_permission = insert_one(permission_data, session)
        await set_binnacle_data(Permission(**new_permission.__dict__).dict())
        return new_permission
    except (GeneralException, PermissionException) as cie:
        raise HTTPException(**cie.__dict__)


async def delete_permission(
    permission_id: int, session: Session = Depends(get_db)
) -> Permission:
    try:
        permission_removed = remove_one(permission_id, session)
        await set_binnacle_data({}, Permission(**permission_removed.__dict__).dict())
        return permission_removed
    except GeneralException as cie:
        raise HTTPException(**cie.__dict__)


async def get_all_permissions(session: Session = Depends(get_db)) -> List[Permission]:
    return get_all(session)


async def get_one_permission(
    permission_id: int, session: Session = Depends(get_db)
) -> Permission:
    try:
        permission_data = get_one(permission_id, session)
        return permission_data
    except GeneralException as cie:
        raise HTTPException(**cie.__dict__)


async def update_permission(
    permission_data: CreateAndUpdatePermission,
    permission_id: int,
    session: Session = Depends(get_db),
) -> Permission:
    try:
        permission_updated = update_one(permission_data, permission_id, session)
        await set_binnacle_data(Permission(**permission_updated.__dict__).dict())
        return permission_updated
    except GeneralException as cie:
        raise HTTPException(**cie.__dict__)

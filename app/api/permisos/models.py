from sqlalchemy import Column, Integer, String
from sqlalchemy.orm import relationship

from ...db.config import Base


class PermissionModel(Base):
    __tablename__ = "permiso"

    id = Column(Integer, primary_key=True, index=True)
    nombre = Column(String(50), nullable=False)

    meta_info = relationship("PermissionRolModel", back_populates="permiso")

from pydantic import BaseModel


class CreateAndUpdateTipoLog(BaseModel):
    url: str
    codigo_estado: int
    ip_atacante: str
    contenido_error: str
    id_usuario: int


class TipoLog(CreateAndUpdateTipoLog):
    id: int

    class Config:
        orm_mode = True

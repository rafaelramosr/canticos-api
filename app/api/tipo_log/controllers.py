from fastapi import Depends
from sqlalchemy.orm import Session
from typing import List

from ...db.config import get_db
from .schemas import TipoLog
from .ops import get_all


async def get_all_tipo_log(session: Session = Depends(get_db)) -> List[TipoLog]:
    return get_all(session)

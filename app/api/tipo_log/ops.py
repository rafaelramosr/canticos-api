from sqlalchemy.orm import Session
from typing import List
from contextlib import contextmanager

from app.config import settings
from app.db.config import get_db
from .models import TipoLogModel
from .schemas import CreateAndUpdateTipoLog, TipoLog


def get_all(session: Session) -> List[TipoLog]:
    return session.query(TipoLogModel).all()


def create_log():
    new_log = TipoLogModel(
        **CreateAndUpdateTipoLog(
            url=settings.log["current_url"],
            codigo_estado=settings.log["status_code"],
            ip_atacante=settings.binnacle["ip_address"],
            contenido_error=settings.log["error_content"],
            id_usuario=settings.binnacle["user_id"],
        ).dict()
    )

    with contextmanager(get_db)() as session:
        session.add(new_log)
        session.commit()
        session.refresh(new_log)

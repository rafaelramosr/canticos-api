from fastapi import APIRouter, Depends
from typing import List

from ...helpers.token import require_token
from .schemas import TipoLog
from . import controllers


r_tipo_log = APIRouter(
    prefix="/logs",
    tags=["logs"],
    dependencies=[Depends(require_token)],
    responses={404: {"description": "Not found"}},
)


@r_tipo_log.get("/")
async def get_all_tipo_log(
    tipo_log: List[TipoLog] = Depends(controllers.get_all_tipo_log),
):
    return tipo_log

from sqlalchemy import Column, ForeignKey, Integer, JSON, String

from ...db.config import Base


class TipoLogModel(Base):
    __tablename__ = "tipo_log"

    id = Column(Integer, primary_key=True, index=True)
    url = Column(String(255), nullable=False)
    codigo_estado = Column(Integer, nullable=False)
    ip_atacante = Column(String(30), nullable=False)
    contenido_error = Column(JSON, nullable=False)
    id_usuario = Column(Integer, ForeignKey("usuario.id"), nullable=False)

from pydantic import BaseModel


class CreateAndUpdateBitacoras(BaseModel):
    tabla: str
    fecha_modificacion: str
    ip_modificacion: str
    datos_anteriores: str
    datos_nuevos: str
    id_accion_bitacora: int
    id_usuario: int


class Bitacoras(CreateAndUpdateBitacoras):
    id: int

    class Config:
        orm_mode = True

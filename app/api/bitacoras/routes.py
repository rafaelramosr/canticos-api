from fastapi import APIRouter, Depends
from typing import List

from ...helpers.token import require_token
from .schemas import Bitacoras
from . import controllers


r_bitacoras = APIRouter(
    prefix="/bitacoras",
    tags=["bitacoras"],
    dependencies=[Depends(require_token)],
    responses={404: {"description": "Not found"}},
)


@r_bitacoras.get("/")
async def get_all_bitacoras(
    bitacoras: List[Bitacoras] = Depends(controllers.get_all_bitacoras),
):
    return bitacoras

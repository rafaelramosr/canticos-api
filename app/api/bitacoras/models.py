from sqlalchemy.types import Date
from sqlalchemy import Column, ForeignKey, Integer, JSON, String

from ...db.config import Base


class BitacoraModel(Base):
    __tablename__ = "bitacora"

    id = Column(Integer, primary_key=True, index=True)
    tabla = Column(String(100), nullable=False)
    fecha_modificacion = Column((Date), nullable=False)
    ip_modificacion = Column(String(30), nullable=False)
    datos_anteriores = Column(JSON)
    datos_nuevos = Column(JSON)
    id_accion_bitacora = Column(
        Integer, ForeignKey("accion_bitacora.id"), nullable=False
    )
    id_usuario = Column(Integer, ForeignKey("usuario.id"), nullable=False)

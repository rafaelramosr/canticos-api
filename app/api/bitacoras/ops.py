from sqlalchemy.orm import Session
from typing import List
from datetime import datetime
from contextlib import contextmanager

from app.config import settings
from app.db.config import get_db
from .models import BitacoraModel
from .schemas import Bitacoras, CreateAndUpdateBitacoras


def get_all(session: Session) -> List[Bitacoras]:
    return session.query(BitacoraModel).all()


def insert_binnacle():
    new_bitacora = BitacoraModel(
        **CreateAndUpdateBitacoras(
            tabla=settings.binnacle["table"],
            fecha_modificacion=datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
            ip_modificacion=settings.binnacle["ip_address"],
            datos_anteriores=settings.binnacle["old_data"],
            datos_nuevos=settings.binnacle["new_data"],
            id_accion_bitacora=settings.binnacle["action_type"],
            id_usuario=settings.binnacle["user_id"],
        ).dict()
    )

    with contextmanager(get_db)() as session:
        session.add(new_bitacora)
        session.commit()
        session.refresh(new_bitacora)

from fastapi import Depends, HTTPException
from sqlalchemy.orm import Session
from typing import List

from ...db.config import get_db
from ...exceptions.general import GeneralException
from .schemas import Bitacoras
from .ops import get_all, insert_binnacle


async def get_all_bitacoras(session: Session = Depends(get_db)) -> List[Bitacoras]:
    return get_all(session)


async def create_binnacle():
    try:
        insert_binnacle()
    except GeneralException as cie:
        raise HTTPException(**cie.__dict__)

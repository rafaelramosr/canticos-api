from fastapi import APIRouter, Depends
from typing import List

from ...helpers.token import require_token
from .schemas import CreateUser, UpdateUser, User
from . import controllers


r_users = APIRouter(
    prefix="/usuarios",
    tags=["usuarios"],
    dependencies=[Depends(require_token)],
    responses={404: {"description": "Not found"}},
)


@r_users.delete("/{user_id}")
async def remove_user(user: User = Depends(controllers.remove_user)):
    return user


@r_users.get("/")
async def get_all_users(
    users: List[User] = Depends(controllers.get_all_users),
):
    return users


@r_users.get("/{user_id}")
async def get_one_user(user: User = Depends(controllers.get_one_user)):
    return user


@r_users.post("/")
async def create_user(user_data: CreateUser = Depends(controllers.create_user)):
    return user_data


@r_users.put("/{user_id}")
async def update_user(user_data: UpdateUser = Depends(controllers.update_user)):
    return user_data

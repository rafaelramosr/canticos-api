from sqlalchemy.orm import Session
from typing import List
from contextlib import contextmanager

from app.config import settings
from ...db.config import get_db
from ...exceptions.general import AlreadyExistError, NotFoundError
from .models import UserModel
from .schemas import CreateUser, UpdateUser, User


def get_all(session: Session) -> List[CreateUser]:
    return session.query(UserModel).all()


def get_one(id: int, session: Session) -> CreateUser:
    user_data = session.query(UserModel).get(id)

    if user_data is None:
        raise NotFoundError

    return user_data


async def get_user_id(email: str) -> int:
    with contextmanager(get_db)() as session:
        user_data: User = session.query(UserModel).filter_by(email=email).first()

    if user_data is None:
        raise NotFoundError

    return user_data.__dict__["id"]


def insert_one(user_data: CreateUser, session: Session) -> CreateUser:
    user_exists = (
        session.query(UserModel).filter(UserModel.email == user_data.email).first()
    )

    if user_exists is not None:
        raise AlreadyExistError

    new_user = UserModel(**user_data.dict())
    session.add(new_user)
    session.commit()
    session.refresh(new_user)
    return new_user


def remove_one(id: int, session: Session) -> CreateUser:
    user_removed = get_one(id, session)

    if user_removed is None:
        raise NotFoundError

    session.delete(user_removed)
    session.commit()

    return user_removed


def update_one(user_data: UpdateUser, id: int, session: Session) -> CreateUser:
    user_updated = get_one(id, session)
    settings.binnacle["old_data"] = CreateUser(**user_updated.__dict__).dict()

    if user_updated is None:
        raise NotFoundError

    if user_updated.email != user_data.email:
        user_exists = (
            session.query(UserModel).filter(UserModel.email == user_data.email).all()
        )

        if len(user_exists) > 0:
            raise AlreadyExistError

    user_updated.nombre = user_data.nombre
    user_updated.apellido = user_data.apellido
    user_updated.email = user_data.email
    user_updated.rol_id = user_data.rol_id

    session.commit()
    session.refresh(user_updated)

    return user_updated

from fastapi import Depends, HTTPException
from sqlalchemy.orm import Session
from typing import List

from app.config import settings
from ...helpers.set_log_data import set_binnacle_data
from ...db.config import get_db
from ...exceptions.general import GeneralException
from ...helpers.avatar import get_image
from .schemas import CreateUser, UpdateUser, User
from . import ops, utils


async def create_user(
    user_data: CreateUser, session: Session = Depends(get_db)
) -> User:
    try:
        if user_data.rol_id == 0:
            user_data.rol_id = settings.deafult_author_rol

        user_data.password = utils.get_password_hash(user_data.password)
        user_data.url = (get_image(user_data.email), user_data.url)[bool(user_data.url)]
        new_user = ops.insert_one(user_data, session)
        await set_binnacle_data(CreateUser(**new_user.__dict__).dict())
        return new_user
    except GeneralException as cie:
        raise HTTPException(**cie.__dict__)


async def get_all_users(session: Session = Depends(get_db)) -> List[User]:
    users = ops.get_all(session)
    return [User(**user.__dict__) for user in users]


async def get_one_user(user_id: int, session: Session = Depends(get_db)) -> User:
    try:
        user_data: User = ops.get_one(user_id, session)
        return User(**user_data.__dict__)
    except GeneralException as cie:
        raise HTTPException(**cie.__dict__)


async def remove_user(user_id: int, session: Session = Depends(get_db)) -> User:
    try:
        user_removed = ops.remove_one(user_id, session)
        await set_binnacle_data({}, CreateUser(**user_removed.__dict__).dict())
        return User(**user_removed.__dict__)
    except GeneralException as cie:
        raise HTTPException(**cie.__dict__)


async def update_user(
    user_data: UpdateUser, user_id: int, session: Session = Depends(get_db)
) -> User:
    try:
        user_updated = ops.update_one(user_data, user_id, session)
        await set_binnacle_data(CreateUser(**user_updated.__dict__).dict())
        return User(**user_updated.__dict__)
    except GeneralException as cie:
        raise HTTPException(**cie.__dict__)

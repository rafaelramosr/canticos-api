from pydantic import BaseModel
from typing import Optional

from app.config import settings


class UpdateUser(BaseModel):
    nombre: str
    apellido: str
    email: str
    rol_id: Optional[int] = settings.deafult_user_rol
    url: Optional[str] = ""


class CreateUser(UpdateUser):
    password: str


class User(UpdateUser):
    id: int

    class Config:
        orm_mode = True

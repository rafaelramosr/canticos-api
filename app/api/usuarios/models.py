from sqlalchemy import Column, ForeignKey, Integer, String
from sqlalchemy.orm import relationship

from ...db.config import Base


class UserModel(Base):
    __tablename__ = "usuario"

    id = Column(Integer, primary_key=True, index=True)
    nombre = Column(String(50), nullable=False)
    apellido = Column(String(50), nullable=False)
    email = Column(String(150), nullable=False)
    password = Column(String(150), nullable=False)
    url = Column(String(250), nullable=False)
    rol_id = Column(Integer, ForeignKey("rol.id"), nullable=False)

    meta_info = relationship("SongModel", back_populates="usuario")

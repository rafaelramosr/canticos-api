from libgravatar import Gravatar


def get_image(email: str):
    g = Gravatar(email)
    return g.get_image()

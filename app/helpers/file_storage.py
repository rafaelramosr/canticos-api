from boto3 import session
from functools import lru_cache
from tempfile import SpooledTemporaryFile
from typing import IO, Union

from ..config import settings
from ..exceptions.s3 import S3ServiceUnavailable


@lru_cache()
def get_s3_client():
    new_session = session.Session()
    return new_session.client(
        "s3",
        region_name=settings.s3_region_name,
        endpoint_url=settings.s3_endpoint_url,
        aws_access_key_id=settings.aws_access_key_id,
        aws_secret_access_key=settings.aws_secret_access_key,
    )


def generate_url(object_key: str) -> str:
    client = get_s3_client()
    url = client.generate_presigned_url(
        ClientMethod="get_object",
        Params={"Bucket": settings.s3_bucket_name, "Key": object_key},
    )

    return url


def remove_file(object_key: str):
    try:
        client = get_s3_client()
        res: dict = client.delete_object(Bucket=settings.s3_bucket_name, Key=object_key)

        response_info = res.get("ResponseMetadata", {})

        if response_info.get("HTTPStatusCode") > 400:
            raise S3ServiceUnavailable
    except:
        raise S3ServiceUnavailable


def update_file(old_key: str, new_key: str, new_category: int) -> str:
    try:
        client = get_s3_client()
        res: dict = client.copy_object(
            ACL="private",
            Bucket=settings.s3_bucket_name,
            ContentType="audio/mpeg",
            CopySource={"Bucket": settings.s3_bucket_name, "Key": old_key},
            Key=new_key,
            Metadata={"category": str(new_category)},
            MetadataDirective="REPLACE",
            TaggingDirective="REPLACE",
        )

        response_info = res.get("ResponseMetadata", {})

        if response_info.get("HTTPStatusCode") > 400:
            raise S3ServiceUnavailable

        if old_key != new_key:
            remove_file(old_key)

        return generate_url(new_key)
    except:
        raise S3ServiceUnavailable


def upload_file(
    file: Union[SpooledTemporaryFile, IO],
    key: str,
    content_type: str,
    category_id: int,
) -> str:
    try:
        client = get_s3_client()
        res: dict = client.put_object(
            ACL="public-read",
            Body=file,
            Bucket=settings.s3_bucket_name,
            ContentType=content_type,
            Key=key,
            Metadata={"category": str(category_id)},
        )

        response_info = res.get("ResponseMetadata", {})

        if response_info.get("HTTPStatusCode") != 200:
            raise S3ServiceUnavailable

        return generate_url(key)
    except:
        raise S3ServiceUnavailable

from fastapi import Depends, HTTPException, Request
from fastapi.security import OAuth2PasswordBearer
from datetime import datetime, timedelta
from jose import jwt, JWTError, ExpiredSignatureError
from typing import List

from ..config import settings
from ..exceptions.token import (
    ExpiredTokenError,
    InvalidCredentialsError,
    TokenException,
)


reusable_oauth2 = OAuth2PasswordBearer(tokenUrl="login")


def decode_token(token: str) -> dict:
    try:
        return jwt.decode(
            token, settings.secret_key, algorithms=[settings.algorithm_token]
        )
    except ExpiredSignatureError:
        raise ExpiredTokenError
    except JWTError:
        raise InvalidCredentialsError


def require_token(request: Request, token: str = Depends(reusable_oauth2)):
    try:
        payload = decode_token(token)

        if not payload:
            raise InvalidCredentialsError

        path = request.get("path").split("/")[1]
        permission = settings.type_permissions[request.method]
        current_permission = f"{path}:{permission}"

        if current_permission not in payload.get("scopes"):
            raise InvalidCredentialsError

        if request.method != "GET":
            settings.binnacle["action_type"] = settings.action_binnacle_id[request.method]
            settings.binnacle["ip_address"] = request.client.host
            settings.binnacle["table"] = path.upper()
            settings.binnacle["user_email"] = payload["sub"]
            settings.isLoggin = True

    except TokenException as cie:
        raise HTTPException(**cie.__dict__)


def create_access_token(user_email: str, user_permissions: List[str]) -> str:
    expires_delta = timedelta(minutes=settings.token_expire_minutes)
    expire = datetime.utcnow() + expires_delta
    to_encode = {"sub": user_email, "scopes": user_permissions, "exp": expire}

    return jwt.encode(
        to_encode, settings.secret_key, algorithm=settings.algorithm_token
    )

import json
from app.config import settings
from app.api.bitacoras.controllers import create_binnacle
from app.api.usuarios.ops import get_user_id
from app.api.tipo_log.ops import create_log


async def set_binnacle_data(new_value, old_value = None):
    old_value = old_value if old_value != None else settings.binnacle["old_data"]

    settings.binnacle["new_data"] = json.dumps(new_value)
    settings.binnacle["old_data"] = json.dumps(old_value)

    user_id = await get_user_id(settings.binnacle["user_email"])
    settings.binnacle["user_id"] = user_id
    await create_binnacle()
    settings.binnacle["old_data"] = {}
    settings.isLoggin = False


async def set_log_data(current_url: str, error_content: str, status_code: int):
    if settings.isLoggin == True:
        settings.log["current_url"] = current_url
        settings.log["error_content"] = json.dumps(error_content)
        settings.log["status_code"] = status_code

        user_id = await get_user_id(settings.binnacle["user_email"])
        settings.binnacle["user_id"] = user_id
        create_log()

    settings.isLoggin = False

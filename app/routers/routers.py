from fastapi import APIRouter

from ..api.accion_bitacoras.routes import r_accion_bitacoras
from ..api.bitacoras.routes import r_bitacoras
from ..api.categorias.routes import r_categories
from ..api.departamentos.routes import r_departments
from ..api.login.routes import r_login
from ..api.municipios.routes import r_municipalities
from ..api.permiso_rol.routes import r_permission_rol
from ..api.permisos.routes import r_permissions
from ..api.register.routes import r_register
from ..api.roles.routes import r_roles
from ..api.canciones.routes import r_songs
from ..api.tipo_log.routes import r_tipo_log
from ..api.usuarios.routes import r_users

router = APIRouter()

router.include_router(r_accion_bitacoras)
router.include_router(r_bitacoras)
router.include_router(r_categories)
router.include_router(r_departments)
router.include_router(r_login)
router.include_router(r_municipalities)
router.include_router(r_permission_rol)
router.include_router(r_permissions)
router.include_router(r_register)
router.include_router(r_roles)
router.include_router(r_songs)
router.include_router(r_tipo_log)
router.include_router(r_users)
